
import Forecast from "../components/Forecast";
import { useState } from "react";
import Button from '@mui/material/Button';
import  Grid  from '@mui/material/Grid';
import { Typography,Box, Container } from "@mui/material";


function ForecastPage(){
      
    const [ name,setName ] = useState("");
    const [ foretellResult,setforetellResult ] = useState("");
    const [ translateResuit , setTransteresult ]  =useState(""); 
    
    const [ ole, setOle] = useState("");
    const [ year, setYear] = useState("");

    function CalculateB(){
        let o = parseFloat(ole);
        let y = parseFloat(year);
        let foretell = y / (o*0.5);
        setforetellResult (foretell);
        if (foretell < 20){
            setTransteresult("โชคดี");
         } else{
                setTransteresult("โชคร้าย");
            }
    }
    
return(
      <Container maxWidth='lg'>
          <Grid container spacing={2} sx= {{ marginTop :"10px"}}>
          <Grid item xs={12}>
                  <Typography variant="h5">
                  ยินดีต้อนรับสู้เว็บพยากรณ์ดวง
                  </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Box sx={{textAlign :'left'}}>
                    คุณชื่อ:<input type="text" 
                                value={name}
                                onChange={(e) =>{ setName(e.target.value); } } /> 
                                <br />
                                <br />
                    อายุ:<input type="text"
                                value={ ole }
                                onChange={(e) =>{ setOle(e.target.value); } } /> 
                                 <br />
                                 <br />
                    ปี:<input type="text"
                                value={ year }
                                onChange={(e) =>{ setYear(e.target.value); } } /> 
                                 <br />
                                 <br />  
                    
                    <Button variant="contained"  onClick={ () =>{ CalculateB()}}>
                    foretell
                    </Button>
                    </Box>
                </Grid>       
                      
                <Grid item xs={4}>
                         
                    <Forecast
                            name = { name }
                            foretell = { foretellResult }
                            result = { translateResuit }
                        
                        />
                 </Grid>    
                </Grid>
         </Container>
    );
}
export default ForecastPage;