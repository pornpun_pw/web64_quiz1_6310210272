import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import Header from './components/Header';

import {Routes,Route} from "react-router-dom";

import ForecastPage from './pages/ForecastPage';


function App() {
  return (
    <div className="App">
      <Header />
        <Routes>

          <Route path="about" element={
                  <AboutUsPage/>
                  }/>
          <Route path="/" element={
                  <ForecastPage/>
                  }/>
        
        </Routes>
    </div>
  );
}

export default App;
